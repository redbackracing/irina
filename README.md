# Redback Racing UNSW #

## Irina (Mobile Application 2017) ##
By Yubai Jiang 
Created 19/02/2017

This version is mainly try do develop a layout for the future app.

### Layouts ###

* Screen layout file
<\MobileAppPrototypeV0.1\app\src\main\res\layout\content_main.xml>;

* Channel list file
<\MobileAppPrototypeV0.1\app\src\main\res\menu\activit_main_drawer.xml>;

* Function list file 
<\MobileAppPrototypeV0.1\app\src\main\res\menu\main.xml>;

* Tool bar file
<\MobileAppPrototypeV0.1\app\src\main\res\layout\app_bar_main.xml>;

* Please refer to wireframes on Taiga wiki page <https://tree.taiga.io/project/mchakravarthy-redback-data-acquisition/wiki/mobile-app-wireframe>.